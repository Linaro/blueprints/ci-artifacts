# BP CI Static Artifacts Used by Tests

A central place for BP CI tests to get the required artifacts that are mostly
static and not generated by the TRS build system.

## Binary files and Git LFS

GitLab supports Git LFS to properly track binary files. Make sure to follow
[GitLab LFS documentation](https://docs.gitlab.com/ee/topics/git/lfs/) when
binary files are added to the repository.

### zynqmp-kria-starter_invalid_sig.capsule

`zynqmp-kria-starter_invalid_sig.capsule` is a copy from
`zynqmp-kria-starter_fw.capsule` and was manually edited with an hex editor. In
this case, it was edited "GCC 13.2.1" string was edited, only changing the last
character with a "2." In theory, any string change will make an invalid
capsule.
